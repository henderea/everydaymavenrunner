class MainWindow < NSWindowController
  def init
    initWithWindowNibName 'MainWindow'
    @view = self.contentView
    self
  end

  def viewDidLoad
    @display_field = @view.viewWithTag 1
  end
end